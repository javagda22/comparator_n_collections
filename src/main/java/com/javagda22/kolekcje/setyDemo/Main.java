package com.javagda22.kolekcje.setyDemo;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Integer> lst = new ArrayList();
        lst.add(5);
        lst.add(5);
        lst.add(1);
        lst.add(4);
        lst.add(3);
        lst.add(6);
        lst.add(6);
        lst.add(7);
        lst.add(3);
        lst.add(22);
        lst.add(3);
        lst.add(5);
        lst.add(7);

        Set<Integer> set = new HashSet<>(Arrays.asList(10, 12, 10, 3, 4, 12, 12, 300));
        System.out.println(lst); // 13
        System.out.println(lst.size());

        Set<Integer> secik = new HashSet<>(lst);

        System.out.println(secik);
        System.out.println(secik.size());

        List<Integer> nowy = new ArrayList<>(secik);

        System.out.println(nowy);

        Set<Integer> secik2 = new TreeSet<>(lst);

    }

}
