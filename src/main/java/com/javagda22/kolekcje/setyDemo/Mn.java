package com.javagda22.kolekcje.setyDemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Mn {
    public static void main(String[] args) {
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer("a", 23, false));
        customers.add(new Customer("b", 15, true));
        customers.add(new Customer("c", 55, false));
        customers.add(new Customer("d", 68, false));
        customers.add(new Customer("e", 63, false));
        customers.add(new Customer("f", 25, false));
        customers.add(new Customer("g", 29, true));
        customers.add(new Customer("h", 99, false));
        customers.add(new Customer("i", 123, true));

        Collections.sort(customers, new Customer.CustomerComparator() );
        System.out.println(customers);
    }

    public static class Customer{
        public String imie;
        public int age;
        public boolean pregnat;

        public Customer(String imie, int age, boolean pregnat) {
            this.imie = imie;
            this.age = age;
            this.pregnat = pregnat;
        }

        @Override
        public String toString() {
            return "Customer{" +
                    "imie='" + imie + '\'' +
                    ", age=" + age +
                    ", pregnat=" + pregnat +
                    '}';
        }

        public static class CustomerComparator implements Comparator<Customer> {
            public int compare(Customer o1, Customer o2) {
                if(o1.pregnat) return -1;
                if(o2.pregnat) return 1;
                if(o2.age == o1.age){
                    return 0;
                }else {
                    if(o1.age > 60 && o2.age > 60){
                        if(o1.age > o2.age) return -1;
                        if(o1.age < o2.age) return 1;
                    }
                    if(o2.age > 60) return 1;
                    if(o1.age > 60) return -1;
                    else return 0;
                }
            }
        }
    }
}


