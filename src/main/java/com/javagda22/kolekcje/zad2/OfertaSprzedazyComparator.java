package com.javagda22.kolekcje.zad2;

import java.util.Comparator;

public class OfertaSprzedazyComparator implements Comparator<OfertaSprzedazy> {
    private boolean czyRosnaco;

    public OfertaSprzedazyComparator(boolean czyRosnaco) {
        this.czyRosnaco = czyRosnaco;
    }

    public int compare(OfertaSprzedazy o1, OfertaSprzedazy o2) {

        if (czyRosnaco) {
            if (o1.getCena() > o2.getCena()) {
                return 1;
            } else if (o1.getCena() < o2.getCena()) {
                return -1;
            }
        } else {
            if (o1.getCena() > o2.getCena()) {
                return -1;
            } else if (o1.getCena() < o2.getCena()) {
                return 1;
            }
        }

        return 0;
    }
}
