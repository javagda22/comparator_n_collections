package com.javagda22.kolekcje.zad2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<OfertaSprzedazy> ofertaSprzedazyList  = new ArrayList();
        ofertaSprzedazyList.add(new OfertaSprzedazy("a1", 1.0));
        ofertaSprzedazyList.add(new OfertaSprzedazy("a2", 23.0));
        ofertaSprzedazyList.add(new OfertaSprzedazy("a3", 17.0));
        ofertaSprzedazyList.add(new OfertaSprzedazy("a4", 50));
        ofertaSprzedazyList.add(new OfertaSprzedazy("a5", 55.0));
        ofertaSprzedazyList.add(new OfertaSprzedazy("a6", 5.0));
        ofertaSprzedazyList.add(new OfertaSprzedazy("a7", 3.0));
        ofertaSprzedazyList.add(new OfertaSprzedazy("a8", 25.0));
        ofertaSprzedazyList.add(new OfertaSprzedazy("a9", 13.0));

        System.out.println(ofertaSprzedazyList);

        System.out.println();
        Collections.sort(ofertaSprzedazyList,new OfertaSprzedazyComparator(true));
        System.out.println(ofertaSprzedazyList);

        System.out.println();
        Collections.sort(ofertaSprzedazyList,new OfertaSprzedazyComparator(false));
        System.out.println(ofertaSprzedazyList);
    }
}
