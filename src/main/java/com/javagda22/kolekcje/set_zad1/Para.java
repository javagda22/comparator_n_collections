package com.javagda22.kolekcje.set_zad1;

import java.util.Objects;

public class Para {
    private int a, b;

    public Para(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Para para = (Para) o;
        return a == para.a &&
                b == para.b;
    }

    @Override
    public int hashCode() {

        return Objects.hash(a, b);
    }

    @Override
    public String toString() {
        return "Para{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
