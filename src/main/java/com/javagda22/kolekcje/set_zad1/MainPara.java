package com.javagda22.kolekcje.set_zad1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MainPara {
    public static void main(String[] args) {
        Set<Para> set = new HashSet<>(Arrays.asList(
                new Para(1, 2),
                new Para(2, 1),
                new Para(1, 1),
                new Para(1, 2)
        ));
        System.out.println(set);
    }
}
