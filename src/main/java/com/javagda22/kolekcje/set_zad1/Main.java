package com.javagda22.kolekcje.set_zad1;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>(Arrays.asList(10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55));
//        List<Integer> integerList = Arrays.asList(10,12,10,3,4,12,12,300,12,40,55);
//        System.out.println(integerList);
//        integerList.add(2000);
//        System.out.println(integerList);

        Set<Integer> integerSet = new HashSet<>(integerList);
        System.out.println(integerList);
        System.out.println(integerSet);
        System.out.println(integerSet.size());
        integerSet.remove(10);
        integerSet.remove(12);
        System.out.println(integerSet);

        Set<Integer> treeTest = new TreeSet<>(integerSet);
        System.out.println(treeTest);

        System.out.println(zawieraDuplikaty("makwrenu")); //false
        System.out.println(zawieraDuplikaty("makarena")); // true
    }

    public static boolean zawieraDuplikaty(String tekst) {
        String[] litery = tekst.split(""); // podział na litery

        // zamiana tablicy liter na listę liter poprzez Arrays.asList
        List<String> listaLiter = new ArrayList<>(Arrays.asList(litery));
        Set<String> zbiorLiter = new HashSet<>(listaLiter);

        return listaLiter.size() != zbiorLiter.size(); // jeśli zawiera duplikaty zwróć true;
//        boolean czyZawiera = true;
//
//        if(listaLiter.size() == zbiorLiter.size()){
//            czyZawiera = false;
//        }else{
//            czyZawiera = true;
//        }
//        return czyZawiera;
    }
}
