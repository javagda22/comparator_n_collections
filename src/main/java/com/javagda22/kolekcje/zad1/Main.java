package com.javagda22.kolekcje.zad1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Osoba> listaOsob = new ArrayList<Osoba>();
        listaOsob.add(new Osoba("a", "b", 1));
        listaOsob.add(new Osoba("a8", "b1", 5));
        listaOsob.add(new Osoba("a7", "b2", 20));
        listaOsob.add(new Osoba("a6", "b3", 13));
        listaOsob.add(new Osoba("a5", "b4", 4));
        listaOsob.add(new Osoba("a4", "b5", 25));
        listaOsob.add(new Osoba("a3", "b6", 18));
        listaOsob.add(new Osoba("a2", "b7", 28));
        listaOsob.add(new Osoba("a1", "b8", 2));

        System.out.println(listaOsob);
        Collections.sort(listaOsob, new OsobaComparator());
        System.out.println(listaOsob);
    }
}
