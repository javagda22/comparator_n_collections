package com.javagda22.kolekcje.zad3;

import java.util.Comparator;

public class DruzynaPozycjaComparator implements Comparator<DruzynaPilkarska> {
    public int compare(DruzynaPilkarska o1, DruzynaPilkarska o2) {
        if (o1.getPozycja() > o2.getPozycja()) {
            return 1;
        } else if (o2.getPozycja() > o1.getPozycja()) {
            return -1;
        }
        return 0;
    }
}
