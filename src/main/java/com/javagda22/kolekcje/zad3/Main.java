package com.javagda22.kolekcje.zad3;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<DruzynaPilkarska> druzynaPilkarskaList = new ArrayList<>();
        druzynaPilkarskaList.add(new DruzynaPilkarska(1, 2, "a"));
        druzynaPilkarskaList.add(new DruzynaPilkarska(3, 10, "b"));
        druzynaPilkarskaList.add(0, new DruzynaPilkarska(6, 5, "c"));
        druzynaPilkarskaList.add(3, new DruzynaPilkarska(5, 7, "d"));
        druzynaPilkarskaList.add(new DruzynaPilkarska(7, 3, "e"));
        druzynaPilkarskaList.add(new DruzynaPilkarska(8, 6, "f"));

        System.out.println();
        Collections.sort(druzynaPilkarskaList, new DruzynaSilaComparator());
        System.out.println(druzynaPilkarskaList);


        System.out.println();
        Collections.sort(druzynaPilkarskaList, new DruzynaPozycjaComparator());
        System.out.println(druzynaPilkarskaList);

        List<DruzynaPilkarska> lst = new ArrayList<>(druzynaPilkarskaList);
        Set<DruzynaPilkarska> set = new HashSet<>(druzynaPilkarskaList);
    }
}
