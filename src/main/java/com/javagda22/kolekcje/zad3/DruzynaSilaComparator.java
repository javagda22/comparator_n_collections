package com.javagda22.kolekcje.zad3;

import java.util.Comparator;

public class DruzynaSilaComparator implements Comparator<DruzynaPilkarska> {
    public int compare(DruzynaPilkarska o1, DruzynaPilkarska o2) {
        double proporcjaO1 = o1.getSila() / o1.getPozycja();
        double proporcjaO2 = o2.getSila() / o2.getPozycja();

        if (proporcjaO1 > proporcjaO2) {
            return 1;
        } else if (proporcjaO1 < proporcjaO2) {
            return -1;
        }
        return 0;
    }
}
