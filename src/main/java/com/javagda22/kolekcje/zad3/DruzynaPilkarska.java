package com.javagda22.kolekcje.zad3;

public class DruzynaPilkarska {
    private double pozycja;
    private double sila;
    private String nazwa;

    public DruzynaPilkarska(double pozycja, double sila, String nazwa) {
        this.pozycja = pozycja;
        this.sila = sila;
        this.nazwa = nazwa;
    }

    public double getPozycja() {
        return pozycja;
    }

    public void setPozycja(double pozycja) {
        this.pozycja = pozycja;
    }

    public double getSila() {
        return sila;
    }

    public void setSila(double sila) {
        this.sila = sila;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Override
    public String toString() {
        return "DruzynaPilkarska{" +
                "pozycja=" + pozycja +
                ", sila=" + sila +
                ", nazwa='" + nazwa + '\'' +
                '}';
    }
}
